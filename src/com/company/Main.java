package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Bank[] banks = new Bank[3];
        banks[0] = new Bank();
        banks[0].name = "ПриватБанк";
        banks[0].courseUSD = 28.5F;
        banks[0].courseEUR = 31.75F;
        banks[0].courseRUB = 0.365F;

        banks[1] = new Bank();
        banks[1].name = "ПУМБ";
        banks[1].courseUSD = 28.4F;
        banks[1].courseEUR = 31.0F;
        banks[1].courseRUB = 0.360F;

        banks[2] = new Bank();
        banks[2].name = "ОщадБанк";
        banks[2].courseUSD = 26.6F;
        banks[2].courseEUR = 30.95F;
        banks[2].courseRUB = 0.380F;


        String USD = "USD";
        String EUR = "EUR";
        String RUB = "RUB";

        Scanner scan = new Scanner(System.in);
// enter amount of money
        System.out.println(" Введите сумму, которую Вы желаете конвертировать");
        int amount = Integer.parseInt(scan.nextLine());
// enter currency
        System.out.println("введите валюту в которую желаете конвертировать (USD, EUR или RUB)");
        String currency = scan.nextLine();

        System.out.println("Введите название банка, в котором желаете произвести процедуру обмена (ПриватБанк, ОщадБанк или ПУМБ) :");
        String bankName = scan.nextLine();

// convert UAH to user currency
        for (Bank i : banks) {
            if (bankName.equalsIgnoreCase(i.name)) {
                if (USD.equalsIgnoreCase(currency)) {
                    System.out.println(String.format(Locale.US, "Ваши деньги %s: %.2f", USD, amount / i.courseUSD));
                } else if (EUR.equalsIgnoreCase(currency)) {
                    System.out.println(String.format(Locale.US, "Ваши деньги %s: %.2f", EUR, amount / i.courseEUR));
                } else if (RUB.equalsIgnoreCase(currency)) {
                    System.out.println(String.format(Locale.US, "Ваши деньги %s: %.2f", RUB, amount / i.courseRUB));
                } else {
                    System.out.println("что-то пошло не так, проверьте вводимые данные");
                }
            }
        }
    }
}
